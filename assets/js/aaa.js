var monthNames = ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
    "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"
];
var day = [1, 2, 3, 4, 5, 6, 7, 8, 26]

var d = new Date();
if (monthNames[d.getMonth()] === "Juin") {
    if (d.getUTCDate() <= 15) {
        var sary = "assets/img/frere/Jean_de_Croix.jpg";
        var nomFr = "Fr. Jean de la Croix";
        var descFr = "Juan de Yepes Álvarez, Saint Jean de la Croix en religion (en espagnol : Juan de la Cruz), né à Fontiveros le 24 juin 1542 et mort au couvent d'Ubeda le 14 décembre 1591, est un saint mystique espagnol, souvent appelé le « Saint du Carmel ». Il accompagne spirituellement les sœurs du Carmel, avant d'être enfermé par les autorités de l'Ordre qui refusent sa réforme. Il est reconnu comme l'un des plus grands poètes du Siècle d'or espagnol. Il est depuis 1952 le saint patron des poètes espagnols. Certains philosophes s'appuient sur ses écrits pour conceptualiser le détachement.";
        var desciption = "Juan de Yepes Álvarez, Saint Jean de la Croix en religion (en espagnol : Juan de la Cruz), né à Fontiveros le 24 juin 1542 , ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    } else if (d.getUTCDate() <= 28) {
        var sary = "assets/img/frere/Irenée.jpg";
        var nomFr = "Fr. Iréné";
        var descFr = "Saint Irénée de Lyon (en latin Irenaeus Lugdunensis, en grec ancien Εἰρηναῖος Σμυρναῖος : Eirênaĩos « pacifique » Smyrnaĩos « de Smyrne ») est le deuxième évêque de Lyon au IIe siècle entre 177 et 202. Il est un des Pères de l'Église. Il est le premier occidental à réaliser une œuvre de théologien systématique. Défenseur de la véritable gnose, il s'est illustré par sa dénonciation de l'idéologie dualiste et des sectes pseudo-gnostiques qui la professaient. Vénéré comme saint, il est fêté le 28 juin dans l'Église catholique et le 23 août dans l'Église orthodoxe.";
        var desciption = "Saint Irénée de Lyon (en latin Irenaeus Lugdunensis, en grec ancien Εἰρηναῖος Σμυρναῖος : Eirênaĩos « pacifique » Smyrnaĩos « de Smyrne ») ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    } else {
        var sary = "assets/img/frere/Frere_Pierrre.jpg";
        var nomFr = "Fr. Pierre";
        var descFr = "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen), ou Simon-Pierre, est un disciple de Jésus de Nazareth. Il est né au début de l'ère chrétienne en Galilée, ou dans la ville toute proche de Bethsaide en Batanée et mort selon la tradition vers 64-70 lors de la persécution Chrétienne. Il était un des douze Apôtres, parmi lesquels il semblait avoir tenu une position privilégiée. Il est considéré comme saint par les religieux catholiques et orthodoxes, sous le nom de saint Pierre. <br> Selon la tradition du catholicisme et de l'orthodoxie, il est le premier évêque de Rome et fonde ainsi dans la tradition du catholicisme la primauté pontificale dont l'actuel pape est le successeur. Son personnage a suscité un grand nombre d'œuvres artistiques, en particulier dans l'Occident latin.";
        var desciption = "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen),ou Simon-Pierre, ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    }

} else if (monthNames[d.getMonth()] === "Juillet") {
    var sary = "assets/img/frere/Frere_Pierrre.jpg";
    var nomFr = "Fr. Pierre";
    var descFr = "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen), ou Simon-Pierre, est un disciple de Jésus de Nazareth. Il est né au début de l'ère chrétienne en Galilée, ou dans la ville toute proche de Bethsaide en Batanée et mort selon la tradition vers 64-70 lors de la persécution Chrétienne. Il était un des douze Apôtres, parmi lesquels il semblait avoir tenu une position privilégiée. Il est considéré comme saint par les religieux catholiques et orthodoxes, sous le nom de saint Pierre. <br> Selon la tradition du catholicisme et de l'orthodoxie, il est le premier évêque de Rome et fonde ainsi dans la tradition du catholicisme la primauté pontificale dont l'actuel pape est le successeur. Son personnage a suscité un grand nombre d'œuvres artistiques, en particulier dans l'Occident latin.";
    var desciption = "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen),ou Simon-Pierre, ....";
    var image = [
        '<img src=' + sary + ' class="logoM">',
    ].join('\n');
    window.onload = function what() {
        document.getElementById('imageFr').innerHTML = image;
    };
} else if (monthNames[d.getMonth()] === "Septembre") {
    if (d.getUTCDate() <= 15) {

        var sary = "assets/img/frere/jeanchry.jpeg";
        var nomFr = "Fr. Jean Chrysostome";
        var descFr = "Saint Jean Chrysostome, né à Antioche entre 344 et 349, et mort en 407 près de Comana, a été archevêque de Constantinople et l'un des pères de l'Église grecque. Son éloquence est à l'origine de son surnom de « Chrysostome » (littéralement « Bouche d'or »). Cependant, sa rigueur et son zèle réformateur l'ont conduit à l'exil et à la mort. <br>   C'est un saint et un docteur de l'Église catholique, de l'Église orthodoxe et de l'Église copte, fêté le 13 septembre en Occident et le 30 janvier en Orient.";
        var desciption = "13 Septembre <br> Saint Jean Chrysostome, né à Antioche entre 344 et 349, et mort en 407 près de Comana, ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    } else {
        var sary = "assets/img/frere/Jean_Marie.jpg";
        var nomFr = "Fr. Jean-Marie ";
        var descFr = "Saint Jean-Marie (Baptiste Vianney), dit le Curé d'Ars ou le saint Curé d'Ars, est né le 8 mai 1786 à Dardilly, près de Lyon, et mort le 4 août 1859 à Ars-sur-Formans. Il fut curé de la paroisse d'Ars (alors Ars-en-Dombes, aujourd'hui Ars-sur-Formans) pendant 41 ans.<br> Il est nommé patron de tous les curés de l'Univers par le pape Pie XI en 1929. On avait annoncé en 2009 qu'il serait nommé patron de tous les prêtres du monde par Benoît XVI, mais on publia en 2010 que le pape avait changé d'avis.";
        var desciption = "13 Septembre <br> Saint Jean-Marie (Baptiste Vianney), dit le Curé d'Ars ou le saint Curé d'Ars, ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    }

} else if (monthNames[d.getMonth()] === "Octobre") {
    var sary = "assets/img/frere/Bruno.jpg";
    var nomFr = "Fr. Bruno";
    var descFr = "Saint Bruno le Chartreux, appelé aussi Bruno de Cologne, né à Cologne vers 1030, mort le 6 octobre 1101 à l'ermitage de la Torre, aujourd'hui chartreuse de Serra San Bruno en Calabre, est un saint catholique fondateur de l'ordre des Chartreux. Son culte dans l'Église universelle est autorisé le 19 juillet 1514 lorsque le pape Léon X accorde oralement sa béatification, tandis que le pape Grégoire XV introduit la fête de saint Bruno au Missel romain le 17 février 1623.";
    var desciption = "06 Octobre <br> Saint Bruno le Chartreux, appelé aussi Bruno de Cologne, né à Cologne vers 1030, mort le 6 octobre ....";
    var image = [
        '<img src=' + sary + ' class="logoM">',
    ].join('\n');
    window.onload = function what() {
        document.getElementById('imageFr').innerHTML = image;
    };
} else if (monthNames[d.getMonth()] === "Novembre") {
    if (d.getUTCDate() < 10) {
        var sary = "assets/img/frere/charles.jpg";
        var nomFr = "Fr. Charles";
        var descFr = "Saint Charles Borromée, en italien Carlo Borromeo, (Arona, 2 octobre 1538 – Milan, 3 novembre 1584) était un évêque italien du XVIe siècle, cardinal de l'Église, artisan de la Réforme catholique, qui fut canonisé dès 1610 par le pape Paul V.   Fête : 04 novembre.";
        var desciption = "04 Novembre <br> Saint Charles Borromée, en italien Carlo Borromeo, (Arona, 2 octobre 1538 – Milan, 3 novembre 1584), ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    } else if (d.getUTCDate() >= 10) {
        var sary = "assets/img/frere/albert.jpg";
        var nomFr = "Frère Albert";
        var descFr = " Saint Albert le Grand naquit aux environs d’Augsbourg, de parents riches des biens de la fortune. Dès son enfance, il montra dans ses études une rare perspicacité ; le goût des sciences lui fit abandonner les traditions chevaleresques de sa famille et le conduisit à l’université de Padoue, alors très célèbre, où il sut tempérer son ardeur pour l’étude par une vive piété. À l’âge de trente ans, encore incertain de son avenir, mais inspiré par la grâce, il alla se jeter aux pieds de la très Sainte Vierge, et crut entendre la céleste Mère lui dire : \"Quitte le monde et entre dans l’Ordre de Saint-Dominique.\" Dès lors, Albert n’hésita plus, et malgré les résistances de sa famille, il entra au noviciat des Dominicains. Tels furent bientôt ses progrès dans la science et la sainteté, qu’il dépassa ses maîtres eux-mêmes. Muni du titre de docteur en théologie, il fut envoyé à Cologne, où sa réputation lui attira pendant longtemps de nombreux et illustres disciples. Mais un seul suffirait à sa gloire, c’est saint Thomas d’Aquin. Ce jeune religieux, déjà tout plongé dans les plus hautes études théologiques, était silencieux parmi les autres au point d’être appelé par ses condisciples : \"le Boeuf muet de Sicile\". Mais Albert les fit taire en disant : \"Les mugissements de ce boeuf retentiront dans le monde entier.\" De Cologne, Albert fut appelé à l’Université de Paris avec son cher disciple. C’est là que son génie parut dans tout son éclat et qu’il composa un grand nombre de ses ouvrages.Plus tard l’obéissance le ramène en Allemagne comme provincial de son Ordre ; il dit adieu, sans murmurer, à sa cellule, à ses livres, à ses nombreux disciples, et voyage sans argent, toujours à pied, à travers un immense territoire pour visiter les nombreux monastères soumis à sa juridiction. Il était âgé de soixante-sept ans quand il dut se soumettre à l’ordre formel du Pape et accepter, en des circonstances difficiles, le siège épiscopal de Ratisbonne ; là, son zèle infatigable ne fut récompensé que par de dures épreuves où se perfectionna sa vertu. Rendu à la paix dans un couvent de son Ordre, il lui fallut bientôt, à l’âge de soixante-dix ans, reprendre ses courses apostoliques. Enfin il put rentrer définitivement dans la retraite pour se préparer à la mort.On s’étonne que, parmi tant de travaux, de voyages et d’oeuvres de zèle, Albert ait pu trouver le temps d’écrire sur les sciences, la philosophie et la théologie des ouvrages qui ne forment pas moins de vingt et un volumes in-folio, et on peut se demander ce qui a le plus excellé en lui du savant, du saint ou de l’apôtre.Il mourut âgé de quatre-vingt-sept ans, le 15 novembre 1280 ; son corps fut enterré à Cologne dans l’église des Dominicains. Il lui a fallu attendre jusqu’au 16 décembre 1931 les honneurs de la canonisation et l’extension de son culte à l’Église universelle. En proclamant sa sainteté, le pape Pie XI y ajouta le titre si glorieux et si bien mérité de docteur de l’Église. Sa fête a été fixée au 15 novembre, jour de sa mort. De temps immémorial, il était connu sous le nom d’Albert le Grand.";
        var desciption = "15 Novembre Fête de Frère Albert, Saint Albert le Grand naquit aux environs d’Augsbourg, ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    } else if (d.getUTCDate() >= 20) {
        var sary = "assets/img/frere/ed.jpg";
        var nomFr = "Fr. Edmond  ";
        var descFr = "Avant de s'en aller en Terre-Sainte pour y finir ses jours dans la prière et la pénitence, le roi Athelstan1 choisit son neveu Edmond, fils de Ealhere, ealdorman de Kent, qui descendait des anciens rois saxons d'Est-Anglie, pour gouverner ses Etats2. Le jour de la Noël 854, les clercs et les nobles du Norfolk assemblés à Attleborough, élirent pour roi Edmond qui était né à Norbury3 quatorze ans plus tôt ; l’élection fut acceptée par les habitants du Suffolk.Lorsque le roi Edmond débarqua sur la terre de son royaume, il se prosterna pour une longue prière ; quand il se releva, douze fontaines sourdirent de terre.Edmond qui voulut terminer ses études dans la résidence royale d'Attleborough, ne fut couronné dans l'église de Bures (Suffolk) qu'à la Noël 856, par Humbert, ancien conseiller de son prédécesseur et évêque d'Hulme. « Pourvu de cette triple consécration, je décidai d'être utile à la nation des Angles, plutôt que de la commander, en négligeant de faire courber les têtes sous un autre joug que celui du Christ ». Ainsi, Edmond est le premier des saints rois à faire de la sainteté son programme de gouvernement. Abbon parle de « ce que fut sa bonté pour ses sujets, sa rigueur pour les méchants », ajoutant qu'il « était pour les indigents d'une magnifique libéralité, pour les orphelins et les veuves un père plein d'indulgence » ; très attentifs aux affaires de gouvernement, « s'il connaissait mal une affaire, il apportait tous ses soins à l'examiner ; sur la voie royale où il marchait, il ne se détournait ni à droite pour se prévaloir de ses mérites, ni à gauche en s'abandonnant aux défauts de la faiblesse humaine.";
        var desciption = "20 Novembre <br> Edmond le Martyr (mort le 20 novembre 869) est le dernier roi d'Est-Anglie avant la conquête de ce royaume par les Vikings, ....";
        var image = [
            '<img src=' + sary + ' class="logoM">',
        ].join('\n');
        window.onload = function what() {
            document.getElementById('imageFr').innerHTML = image;
        };
    }

} else {
    ObjectFr = [{
            sary: "assets/img/frere/Jean_de_Croix.jpg",
            nomFr: "Fr. Jean de la Croix",
            descFr: "Juan de Yepes Álvarez, Saint Jean de la Croix en religion (en espagnol : Juan de la Cruz), né à Fontiveros le 24 juin 1542 et mort au couvent d'Ubeda le 14 décembre 1591, est un saint mystique espagnol, souvent appelé le « Saint du Carmel ». Il accompagne spirituellement les sœurs du Carmel, avant d'être enfermé par les autorités de l'Ordre qui refusent sa réforme. Il est reconnu comme l'un des plus grands poètes du Siècle d'or espagnol. Il est depuis 1952 le saint patron des poètes espagnols. Certains philosophes s'appuient sur ses écrits pour conceptualiser le détachement.",
            desciption: "Juan de Yepes Álvarez, Saint Jean de la Croix en religion (en espagnol : Juan de la Cruz), né à Fontiveros le 24 juin 1542 , ....",
        },
        {
            sary: "assets/img/frere/Irenée.jpg",
            nomFr: "Fr. Iréné",
            descFr: "Saint Irénée de Lyon (en latin Irenaeus Lugdunensis, en grec ancien Εἰρηναῖος Σμυρναῖος : Eirênaĩos « pacifique » Smyrnaĩos « de Smyrne ») est le deuxième évêque de Lyon au IIe siècle entre 177 et 202. Il est un des Pères de l'Église. Il est le premier occidental à réaliser une œuvre de théologien systématique. Défenseur de la véritable gnose, il s'est illustré par sa dénonciation de l'idéologie dualiste et des sectes pseudo-gnostiques qui la professaient. Vénéré comme saint, il est fêté le 28 juin dans l'Église catholique et le 23 août dans l'Église orthodoxe.",
            desciption: "Saint Irénée de Lyon (en latin Irenaeus Lugdunensis, en grec ancien Εἰρηναῖος Σμυρναῖος : Eirênaĩos « pacifique » Smyrnaĩos « de Smyrne ») ....",

        },
        {
            sary: "assets/img/frere/ed.jpg",
            nomFr: "Fr. Edmond  ",
            descFr: "Edmond le Martyr (mort le 20 novembre 869) est le dernier roi d'Est-Anglie avant la conquête de ce royaume par les Vikings. Considéré comme un saint après sa mort. Edmond est toujours fêté le 20 novembre par l'Église catholique. Il est communément figuré une flèche à la main (par exemple sur le diptyque de Wilton), ou apparaît lié à un arbre, criblé de flèches. Il se distingue de la représentation de saint Sébastien par le port d'une couronne royale et la présence d'un loup. Il est le saint patron de l'abbaye de Douai, du diocèse catholique d'Est-Anglie et du comté de Suffolk. Plusieurs églises lui sont dédiées, dont St Edmund,  King and Martyr à Londres.",
            desciption: "20 Novembre <br> Edmond le Martyr (mort le 20 novembre 869) est le dernier roi d'Est-Anglie avant la conquête de ce royaume par les Vikings, ....",
        },
        {
            sary: "assets/img/frere/charles.jpg",
            nomFr: "Fr. Charles",
            descFr: "Saint Charles Borromée, en italien Carlo Borromeo, (Arona, 2 octobre 1538 – Milan, 3 novembre 1584) était un évêque italien du XVIe siècle, cardinal de l'Église, artisan de la Réforme catholique, qui fut canonisé dès 1610 par le pape Paul V.   Fête : 04 novembre.",
            desciption: "04 Novembre <br> Saint Charles Borromée, en italien Carlo Borromeo, (Arona, 2 octobre 1538 – Milan, 3 novembre 1584), ....",
        },
        {
            sary: "assets/img/frere/Bruno.jpg",
            nomFr: "Fr. Bruno",
            descFr: "Saint Bruno le Chartreux, appelé aussi Bruno de Cologne, né à Cologne vers 1030, mort le 6 octobre 1101 à l'ermitage de la Torre, aujourd'hui chartreuse de Serra San Bruno en Calabre, est un saint catholique fondateur de l'ordre des Chartreux. Son culte dans l'Église universelle est autorisé le 19 juillet 1514 lorsque le pape Léon X accorde oralement sa béatification, tandis que le pape Grégoire XV introduit la fête de saint Bruno au Missel romain le 17 février 1623.",
            desciption: "06 Octobre <br> Saint Bruno le Chartreux, appelé aussi Bruno de Cologne, né à Cologne vers 1030, mort le 6 octobre ....",

        },
        {
            sary: "assets/img/frere/Jean_Marie.jpg",
            nomFr: "Fr. Jean-Marie ",
            descFr: "Saint Jean-Marie (Baptiste Vianney), dit le Curé d'Ars ou le saint Curé d'Ars, est né le 8 mai 1786 à Dardilly, près de Lyon, et mort le 4 août 1859 à Ars-sur-Formans. Il fut curé de la paroisse d'Ars (alors Ars-en-Dombes, aujourd'hui Ars-sur-Formans) pendant 41 ans.<br> Il est nommé patron de tous les curés de l'Univers par le pape Pie XI en 1929. On avait annoncé en 2009 qu'il serait nommé patron de tous les prêtres du monde par Benoît XVI, mais on publia en 2010 que le pape avait changé d'avis.",
            desciption: "13 Septembre <br> Saint Jean-Marie (Baptiste Vianney), dit le Curé d'Ars ou le saint Curé d'Ars, ....",

        },
        {
            sary: "assets/img/frere/jeanchry.jpeg",
            nomFr: "Fr. Jean Chrysostome",
            descFr: "Saint Jean Chrysostome, né à Antioche entre 344 et 349, et mort en 407 près de Comana, a été archevêque de Constantinople et l'un des pères de l'Église grecque. Son éloquence est à l'origine de son surnom de « Chrysostome » (littéralement « Bouche d'or »). Cependant, sa rigueur et son zèle réformateur l'ont conduit à l'exil et à la mort. <br>   C'est un saint et un docteur de l'Église catholique, de l'Église orthodoxe et de l'Église copte, fêté le 13 septembre en Occident et le 30 janvier en Orient.",
            desciption: "13 Septembre <br> Saint Jean Chrysostome, né à Antioche entre 344 et 349, et mort en 407 près de Comana, ....",

        },
        {
            sary: "assets/img/frere/Frere_Pierrre.jpg",
            nomFr: "Fr. Pierre",
            descFr: "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen), ou Simon-Pierre, est un disciple de Jésus de Nazareth. Il est né au début de l'ère chrétienne en Galilée, ou dans la ville toute proche de Bethsaide en Batanée et mort selon la tradition vers 64-70 lors de la persécution Chrétienne. Il était un des douze Apôtres, parmi lesquels il semblait avoir tenu une position privilégiée. Il est considéré comme saint par les religieux catholiques et orthodoxes, sous le nom de saint Pierre. <br> Selon la tradition du catholicisme et de l'orthodoxie, il est le premier évêque de Rome et fonde ainsi dans la tradition du catholicisme la primauté pontificale dont l'actuel pape est le successeur. Son personnage a suscité un grand nombre d'œuvres artistiques, en particulier dans l'Occident latin.",
            desciption: "St. PIERRE,  Simon, barjona ou fils de Jonas, aussi appelé Cephas (« le roc », « la pierre » en araméen),ou Simon-Pierre, ....",

        }
    ];
    var item = ObjectFr[Math.floor(Math.random() * ObjectFr.length)];
    var sary = item.sary;
    var nomFr = item.nomFr;
    var descFr = item.descFr;
    var description = item.description;
    var image = [
        '<img src=' + sary + ' class="logoM">',
    ].join('\n');
    window.onload = function what() {
        document.getElementById('imageFr').innerHTML = image;
    };
}

function sendMail() {
    document.getElementById("load").style.display = "none";
    document.getElementById("spinner").style.display = "block";
    var a = document.getElementById('message').value + '\n' + 'Envoyé par' + document.getElementById('email').value
    Email.send({
        Host: "smtp.elasticemail.com",
        Username: "manjatonick@gmail.com",
        Password: "2a71353f-853a-4f69-ab1d-22245d78db59",
        To: 'maromby@moov.mg',
        From: "manjatonick@gmail.com",
        Subject: document.getElementById('subject').value,
        Body: a,
    }).then(
        message => {
            console.log(message)
            if (message === 'OK') {
                Swal.fire({
                    title: 'Email bien envoyé!',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 1500
                });
                location.reload();
            } else {
                Swal.fire({
                    title: 'Erreur d\'envoie!',
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
            document.getElementById("spinner").style.display = "none";
            document.getElementById("load").style.display = "block";
        });
}


function fr() {
    document.getElementById('fr').style.webkitTransition = 'opacity 1s';
    document.getElementById("fr").style.display = 'block';
    document.getElementById("mg").style.display = 'none';
}

function mg() {
    document.getElementById('fr').style.webkitTransition = 'opacity 1s';
    document.getElementById("mg").style.display = 'block';
    document.getElementById("fr").style.display = 'none';
}